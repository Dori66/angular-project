import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Service } from '../service.service';

@Component({
  selector: 'app-form-component',
  templateUrl: './form-component.component.html',
  styleUrls: ['./form-component.component.scss']
})
export class FormComponentComponent implements OnInit {




  formBase = new FormGroup({
    name: new FormControl('', Validators.required),
    username: new FormControl('',Validators.required),
    email : new FormControl('',Validators.required)
  })

  constructor(private service: Service) { }

  ngOnInit(): void {
  }


  onSubmit(){
    this.service.saveData(this.formBase.value).subscribe((_) => alert('Success 204'))
  }

}
