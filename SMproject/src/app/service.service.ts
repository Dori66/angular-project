import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable } from 'rxjs';
import { USER } from './User.model';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Service {

  constructor(private http: HttpClient) { }



  errorHandler(error: HttpErrorResponse){
    if(error.status === 0){
      console.error('An error occured: ', error.error);
    }else{
      console.error(`Backend returned code ${error.status}, body was `,error.error);
    }
    alert('Failed Code 404')
    return throwError(() => new Error('Something bad happend, please try again later'))
  }

  fetchData(id: number): Observable<USER>{
    return this.http.get<USER>(`https://jsonplaceholder.typicode.com/users/${id}`).pipe(
      catchError(this.errorHandler)
    );
  }

  saveData(data: any){
      return this.http.post(`https://jsonplaceholder.typicode.com/users/`,data).pipe(
        catchError(this.errorHandler)
      )
  }




}
