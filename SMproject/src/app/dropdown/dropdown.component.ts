import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Service } from '../service.service';
import { USER } from '../User.model';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit, OnDestroy {

  constructor(private service: Service) { }

  user!: USER;
  sub = new Subscription();

  ngOnInit(): void {
    this.sub.add(
      this.service.fetchData(3).subscribe(user => this.user = user)
    )
  }

  fetchData(event:any){
    this.sub.add(
      this.service.fetchData(event.value).pipe(
        ).subscribe(data => {
         this.user = data;
        }, (error) => console.log(error))
    )
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe;
  }
}
